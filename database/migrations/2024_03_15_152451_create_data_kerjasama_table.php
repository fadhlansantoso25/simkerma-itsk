<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('data_kerjasama', function (Blueprint $table) {
            $table->id();
            $table->date('tanggal_mou');
            $table->date('tanggal_akhir');
            $table->string('deskripsi');
            $table->string('penanggung_jawab');
            $table->string('email');
            $table->string('nama_mitra');
            $table->string('jenis_mitra');
            $table->string('no_surat_instansi');
            $table->string('no_surat_mitra');
            $table->string('file');
            $table->string('jenis_dokumen');
            $table->string('status');
            $table->foreignIdFor(User::class)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('data_kerjasama');
    }
};
