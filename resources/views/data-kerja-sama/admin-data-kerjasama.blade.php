@extends('base')

@section('data-kerjasama')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-md-8">
                <h6 class="page-title">Semua Data Kerja Sama</h6>
                {{-- Breadcrumb --}}
            </div>
            <div class="col-md-4">
                <div class="d-flex flex-column flex-md-row justify-content-end align-items-md-center gap-3">
                    <button type="button" class="btn btn-primary waves-effect waves-light"><i
                            class="mdi mdi-content-save"></i> Export</button>
                    <button type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal"
                        data-bs-target="#modalTambah"><i class="mdi mdi-plus"></i>
                        Tambah Kerja Sama Baru</button>
                </div>
            </div>
        </div>
    </div>
    {{-- Data Tabel --}}
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-rep-plugin">
                        <div class="table-responsive mb-0" data-bs-pattern="priority-columns">
                            <table id="tech-companies-1" class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>ID Kerja Sama</th>
                                        <th data-priority="1">Nama Instansi</th>
                                        <th data-priority="3">Nomor Surat Instansi</th>
                                        <th data-priority="1">Nomor Surat Mitra</th>
                                        <th data-priority="3">Tanggal Mulai</th>
                                        <th data-priority="3">Tanggal Selesai</th>
                                        <th data-priority="1">Edit</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($dataKerjasama as $item)
                                        <tr>
                                            <th><span class="co-name">{{ $loop->iteration }}</span></th>
                                            <td>{{ $item->nama_mitra }}</td>
                                            <td>{{ $item->no_surat_instansi }}</td>
                                            <td>{{ $item->no_surat_mitra }}</td>
                                            <td>{{ $item->tanggal_mou }}</td>
                                            <td>{{ $item->tanggal_akhir }}</td>
                                            <td>
                                                <div class="row gap">
                                                    <div class="col">
                                                        <form action="{{ route('data.destroy', $item->id) }}" method="POST">
                                                            @csrf
                                                            @method('DELETE')        
                                                        <button type="button" class="btn btn-secondary waves-effect"><i
                                                                class="mdi mdi-eye" data-bs-toggle="modal"
                                                                data-bs-target="#modalView{{ $item->id }}"></i></button>
                                                        <button type="button" class="btn btn-warning waves-effect"><i
                                                                class="mdi mdi-pencil" style="color: black"
                                                                data-bs-toggle="modal" data-bs-target="#modalEdit"></i></button>
                                                        <button type="submit" class="btn btn-danger waves-effect">
                                                                
                                                        <i class="mdi mdi-trash-can" style="color: black"></i></button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->

    {{-- MODAL TAMBAH KERJA SAMA BARU --}}
    <div id="modalTambah" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog"
        aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="/add-datakerjasama" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel2">Tambah Kerja Sama Baru</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="col">
                            <div class="col-md-12 pb-3">
                                <label for="nama-lengkap" class="form-label">Nama Lengkap</label>
                                <input type="text" class="form-control" id="nama-lengkap" name="penanggung_jawab" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="alamat-email" class="form-label">Alamat Email</label>
                                <input type="email" class="form-control" id="alamat-email" name="email" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="nama-instansi" class="form-label">Nama Instansi</label>
                                <input type="text" class="form-control" id="nama-instansi" name="nama_mitra" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="nama-kegiatan" class="form-label">Nama Kegiatan</label>
                                <input type="text" class="form-control" id="nama-kegiatan" name="deskripsi" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="judul-mou" class="form-label">Nomor surat mitra</label>
                                <input type="text" class="form-control" id="judul-mou" name="no_surat_mitra" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label class="form-label">Tanggal Mulai</label>
                                <div class="input-group" id="datepicker1">
                                    <input type="date" class="form-control" placeholder="yyyy-mm-dd" name="tanggal_mou">

                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div><!-- input-group -->
                            </div>
                            <div class="col-md-12 pb-3">
                                <label class="form-label">Tanggal Berakhir</label>
                                <div class="input-group" id="datepicker1">
                                    <input type="date" class="form-control" placeholder="yyyy-mm-dd" name="tanggal_akhir">
                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div><!-- input-group -->
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="judul-mou" class="form-label">Dokumen MoU</label>
                                <div class="col-sm-6">
                                    <div class="file-upload-wrapper" data-text="Pilih dokumen MoU">
                                        <input name="file" type="file" class="file-upload-field" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    {{-- MODAL EDIT --}}
    <div id="modalEdit" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog"
        aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel2">Edit Data Kerja Sama</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="col">
                            <div class="col-md-12 pb-3">
                                <label for="nama-lengkap" class="form-label">Nama Lengkap</label>
                                <input type="text" class="form-control" id="nama-lengkap" name="nama-lengkap" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="alamat-email" class="form-label">Alamat Email</label>
                                <input type="email" class="form-control" id="alamat-email" name="alamat-email" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="nama-instansi" class="form-label">Nama Instansi</label>
                                <input type="text" class="form-control" id="nama-instansi" name="instansi" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="nama-kegiatan" class="form-label">Nama Kegiatan</label>
                                <input type="text" class="form-control" id="nama-kegiatan" name="nama-kegiatan" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="judul-mou" class="form-label">Judul MoU</label>
                                <input type="text" class="form-control" id="judul-mou" name="judul-mou" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label class="form-label">Tanggal Mulai</label>
                                <div class="input-group" id="datepicker1">
                                    <input type="text" class="form-control" placeholder="dd M, yyyy"
                                        data-date-format="dd M, yyyy" data-date-container='#datepicker1'
                                        data-provide="datepicker">

                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div><!-- input-group -->
                            </div>
                            <div class="col-md-12 pb-3">
                                <label class="form-label">Tanggal Berakhir</label>
                                <div class="input-group" id="datepicker1">
                                    <input type="text" class="form-control" placeholder="dd M, yyyy"
                                        data-date-format="dd M, yyyy" data-date-container='#datepicker1'
                                        data-provide="datepicker">

                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div><!-- input-group -->
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="judul-mou" class="form-label">Dokumen MoU</label>
                                <div class="col-sm-6">
                                    <div class="file-upload-wrapper" data-text="Pilih dokumen MoU">
                                        <input name="file" type="file" class="file-upload-field" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    {{-- MODAL VIEW --}}
    @foreach ($dataKerjasama as $item)
    <div id="modalView{{ $item->id }}" class="modal fade bs-example-modal-center" tabindex="-1" role="dialog"
        aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <form action="" method="POST">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabel2">Detail Data Kerja Sama</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="col">
                            <div class="col-md-12 pb-3">
                                <label for="nama-lengkap" class="form-label">Nama Lengkap</label>
                                <input type="text" class="form-control" id="nama-lengkap{{ $item->id }}" value="{{ $item->penanggung_jawab }}" name="nama-lengkap" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="alamat-email" class="form-label">Alamat Email</label>
                                <input type="email" class="form-control" id="alamat-email{{ $item->id }}" value="{{ $item->email }}" name="alamat-email" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="nama-instansi" class="form-label">Nama Instansi</label>
                                <input type="text" class="form-control" id="nama-instansi{{ $item->id }}" value="{{ $item->nama_mitra }}" name="instansi" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="nama-kegiatan" class="form-label">Nama Kegiatan</label>
                                <input type="text" class="form-control" id="nama-kegiatan{{ $item->id }}" value="{{ $item->deskripsi }}" name="nama-kegiatan" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="judul-mou" class="form-label">Nomor Surat Mitra</label>
                                <input type="text" class="form-control" id="judul-mou{{ $item->id }}" value="{{ $item->no_surat_mitra }}" name="judul-mou" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label class="form-label">Tanggal Mulai</label>
                                <div class="input-group" id="datepicker1">
                                    <input type="date" class="form-control" value="{{ $item->tanggal_mou }}" placeholder="yyyy-mm-dd">

                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div><!-- input-group -->
                            </div>
                            <div class="col-md-12 pb-3">
                                <label class="form-label">Tanggal Berakhir</label>
                                <div class="input-group" id="datepicker1">
                                    <input type="date" class="form-control" value="{{ $item->tanggal_akhir }}" placeholder="yyyy-mm-dd">

                                    <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                </div><!-- input-group -->
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="judul-mou" class="form-label">Dokumen MoU</label>
                                <div class="col-sm-6">
                                    <div class="file-upload-wrapper" data-text="Pilih dokumen MoU">
                                        <input name="file" type="file" class="file-upload-field" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    @endforeach
@endsection
@if(session('success'))
    @push('script')
        <script>
            alert("{{ session('success') }}")
        </script>
    @endpush
@endif

@push('script')

<script>
    $(document).ready(function(){
    $("#datatable").DataTable({
        lengthChange: true
    });
});
</script>
    
@endpush
