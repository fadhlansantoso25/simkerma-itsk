@extends('base')

@section('admin')
    <div class="row">
        <div class="col-xl-3 col-md-6 ">
            <div class="card mini-stat bg-danger text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-start mini-stat-img me-4 pt-2">
                            <h1>22</h1>
                        </div>
                        <h5 class="font-size-16 text-uppercase text-white-50">SEMUA KERJA SAMA</h5>
                    </div>
                    <div class="pt-2">
                        <div class="float-end">
                            <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                        </div>

                        <p class="text-white-50 mb-0 mt-1">Lihat Selengkapnya</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat bg-warning text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-start mini-stat-img me-4 pt-2">
                            <h1>10</h1>
                        </div>
                        <h5 class="font-size-16 text-uppercase text-white-50">KERJA SAMA AKTIF</h5>
                    </div>
                    <div class="pt-2">
                        <div class="float-end">
                            <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                        </div>

                        <p class="text-white-50 mb-0 mt-1">Lihat Selengkapnya</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat bg-success text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-start mini-stat-img me-4 pt-2">
                            <h1>7</h1>
                        </div>
                        <h5 class="font-size-16 text-uppercase text-white-50">kERJA SAMA BERAKHIR</h5>
                    </div>
                    <div class="pt-2">
                        <div class="float-end">
                            <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                        </div>

                        <p class="text-white-50 mb-0 mt-1">Lihat Selengkapnya</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card mini-stat bg-primary text-white">
                <div class="card-body">
                    <div class="mb-4">
                        <div class="float-start mini-stat-img me-4 pt-2">
                            <h1>5</h1>
                        </div>
                        <h5 class="font-size-16 text-uppercase text-white-50">KERJA SAMA AKAN BERKAHIR</h5>
                    </div>
                    <div class="pt-2">
                        <div class="float-end">
                            <a href="#" class="text-white-50"><i class="mdi mdi-arrow-right h5"></i></a>
                        </div>

                        <p class="text-white-50 mb-0 mt-1">Lihat Selengkapnya</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->

    <div class="row">
        <div class="col-xl">
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title mb-4">Statistik Kerja Sama</h4>

                    <div class="row justify-content-center">
                        <div class="col-sm-4">
                            <div class="text-center">
                                <h5 class="mb-0 font-size-20">22</h5>
                            <p class="text-muted">Masuk</p>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="text-center">
                                <h5 class="mb-0 font-size-20">7</h5>
                            <p class="text-muted">Berakhir</p>
                            </div>
                        </div>
                    </div>

                    <div id="chart"></div>

                </div>
            </div>
        </div> <!-- end col -->
        <div class="col-xl-3">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Jenis Instansi</h4>

                    <div id="ct-donut" class="ct-chart wid"></div>

                    <div class="mt-4">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td><span class="badge bg-primary">A</span></td>
                                    <td>A</td>
                                    <td class="text-end">54.5%</td>
                                </tr>
                                <tr>
                                    <td><span class="badge bg-success">B</span></td>
                                    <td>B</td>
                                    <td class="text-end">28.0%</td>
                                </tr>
                                <tr>
                                    <td><span class="badge bg-warning">C</span></td>
                                    <td>C</td>
                                    <td class="text-end">17.5%</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title mb-4">Aktivitas</h4>
                    <ol class="activity-feed">
                        <li class="feed-item">
                            <div class="feed-item-list">
                                <span class="date">Jan 22</span>
                                <span class="activity-text">MOU dari Mitra D ditambahkan</span>
                            </div>
                        </li>
                        <li class="feed-item">
                            <div class="feed-item-list">
                                <span class="date">Jan 20</span>
                                <span class="activity-text">At vero eos et accusamus et iusto odio
                                    dignissimos ducimus qui deleniti atque...<a href="#"
                                        class="text-success">Read more</a></span>
                            </div>
                        </li>
                        <li class="feed-item">
                            <div class="feed-item-list">
                                <span class="date">Jan 19</span>
                                <span class="activity-text">Joined the group “Boardsmanship
                                    Forum”</span>
                            </div>
                        </li>
                        <li class="feed-item">
                            <div class="feed-item-list">
                                <span class="date">Jan 17</span>
                                <span class="activity-text">Responded to need “In-Kind
                                    Opportunity”</span>
                            </div>
                        </li>
                    </ol>
                    <div class="text-center">
                        <a href="#" class="btn btn-primary">Load More</a>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
    <!-- end row -->


@endsection

@push('script')

    <!-- Morris Chart JS -->
    <script src="assets/libs/morris.js/morris.min.js"></script>
    <script src="assets/libs/raphael/raphael.min.js"></script>
    <script src="assets/js/custom.js"></script>

    <!-- Plugin Js/Chartist JS-->
    <script src="assets/libs/chartist/chartist.min.js"></script>
    <script src="assets/libs/chartist-plugin-tooltips/chartist-plugin-tooltip.min.js"></script>
    <script src="assets/js/pages/dashboard.init.js"></script>
    
@endpush