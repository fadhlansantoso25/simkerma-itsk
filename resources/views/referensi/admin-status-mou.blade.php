@extends('base')

@section('status-mou')
    <div class="page-title-box">
        <div class="row align-items-center">
            <div class="col-md-8">
                <h6 class="page-title">Data Status MoU</h6>
                {{-- Breadcrumb --}}
            </div>
            <div class="col-md-4">
                <div class="d-flex flex-column flex-md-row justify-content-end align-items-md-center gap-3">
                    <button type="button" class="btn btn-primary waves-effect waves-light"><i
                            class="mdi mdi-content-save"></i> Export</button>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-6">
                    <table id="datatable" class="table table-bordered dt-responsive nowrap"
                        style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>ID MoU</th>
                                <th>Nama Instansi</th>
                                <th>Judul MoU</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Tiger Nixon</td>
                                <td>System Architect</td>
                                <td>Edinburgh</td>
                                <td>Tes</td>
                            </tr>
                            <tr>
                                <td>Garrett Winters</td>
                                <td>Accountant</td>
                                <td>Tokyo</td>
                                <td>tes</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                {{-- Chart --}}
                <div class="col-sm-6">
                    <h4 class="card-title mb-4">Jenis Instansi</h4>
        
                    <div id="ct-donut" class="ct-chart wid"></div>
        
                    <div class="mt-4">
                        <table class="table mb-0">
                            <tbody>
                                <tr>
                                    <td><span class="badge bg-primary">A</span></td>
                                    <td>A</td>
                                    <td class="text-end">54.5%</td>
                                </tr>
                                <tr>
                                    <td><span class="badge bg-success">B</span></td>
                                    <td>B</td>
                                    <td class="text-end">28.0%</td>
                                </tr>
                                <tr>
                                    <td><span class="badge bg-warning">C</span></td>
                                    <td>C</td>
                                    <td class="text-end">17.5%</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

<script>
    $(document).ready(function(){
    $("#datatable").DataTable({
        lengthChange: true
    });
});
</script>

<!-- Plugin Js/Chartist JS-->
<script src="assets/libs/chartist/chartist.min.js"></script>
<script src="assets/libs/chartist-plugin-tooltips/chartist-plugin-tooltip.min.js"></script>
<script src="assets/js/pages/dashboard.init.js"></script>
    
@endpush