@extends('base')

@section('template')
    <div class="row align-items-center mb-0">
        <div class="col-md-8">
            <ol class="breadcrumb m-0">
                <li class="breadcrumb-item"><a href="#">SIMKERMA</a></li>
                <li class="breadcrumb-item"><a href="#">Template MoU</a></li>
            </ol>
        </div>
    </div>
    <div class="row align-items-center mb-0">
        <div class="col-md-12">
            <div class="float-end d-md-block">
                <div class="my-3 text-center">
                    <button type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal"
                        data-bs-target="#modelTambah">+ Tambahkan Template MoU</button>
                </div>
                <!-- sample modal content -->
                <div id="modelTambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modelTambahLabel"
                    aria-hidden="true">
                    <div class="modal-dialog">
                        <form action="{{ route('template.store') }}" method="POST" class="modal-content" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-header">
                                <h5 class="modal-title" id="modelTambahLabel">Tambah Template MoU
                                </h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div>
                                    <div class="mb-3">
                                        <label for="namaMoU" class="form-label">Nama MoU</label>
                                        <input type="text" class="form-control" id="namaMoU" name="nama"
                                            placeholder="Masukkan Nama MoU">
                                    </div>
                                    <div class="mb-3">
                                        <div class="mb-3">
                                            <label for="jenisMoU" class="form-label">Status template</label>
                                            <select class="form-control select2" id="jenisMoU" name="is_active">
                                                <option>Pilih Status template</option>
                                                <option value="1">Aktif</option>
                                                <option value="0">Tidak Aktif</option>
                                            </select>
                                        </div>

                                        <div class="col-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    <h4 class="card-title">Unggah Template MoU</h4>
                                                    <div class="mb-5">
                                                        <div class="dropzone">
                                                            <div class="fallback">
                                                                <input name="file" type="file" name="file">
                                                            </div>

                                                            <div class="dz-message needsclick">
                                                                <div class="mb-3">
                                                                    <i
                                                                        class="mdi mdi-cloud-upload display-4 text-muted"></i>
                                                                </div>

                                                                <h4>Drop files here or click to upload.</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger waves-effect"
                                    data-bs-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-primary waves-effect waves-light"><i
                                        class="fas fa-save"></i> Simpan</button>
                            </div>
                        </form>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->
            </div>
        </div>
    </div>
    <!-- end modal tambah -->

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="datatable" class="table table-bordered table-responsive mb-0">
                            <thead>
                                <tr>
                                    <th style="width: 2%;">Nomor</th>
                                    <th style="width: 25%;">Nama MoU</th>
                                    <th style="width: 5%;">Status Template</th>
                                    <th style="width: 5%;">Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($template as $data)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $data->nama }}</td>
                                        <td>
                                            <!-- Button untuk Checklis -->
                                            <div class="form-check form-switch">
                                                <input class="form-check-input toggle-template" type="checkbox"
                                                    id="switch{{ $data->id }}" switch="bool" data-template-id="{{ $data->id }}" {{ $data->is_active ? 'checked' : '' }} />
                                                <label class="form-check-label" for="switch{{ $data->id }}"/>
                                            </div>
                                        </td>
                                        <td>
                                            <!-- Button untuk mengedit -->
                                            <button type="button"
                                                class="ms btn btn-warning btn-sm waves-effect waves-light ms-1 me-1"
                                                style="font-size: 12px; padding: 6px 12px;" data-bs-toggle="modal"
                                                data-bs-target="#myModal"><i class="mdi mdi-pen"
                                                    style="color: black"></i></button>

                                            <!-- Modal untuk mengedit -->
                                            <div id="myModal" class="modal fade" tabindex="" role="dialog"
                                                aria-labelledby="myModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <form action="{{ route('template.update', $data->id) }}" method="POST" class="modal-content" enctype="multipart/form-data">
                                                        @csrf
                                                        @method('PUT')
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="myModalLabel">Edit data MoU
                                                            </h5>
                                                            <button type="button" class="btn-close"
                                                                data-bs-dismiss="modal" aria-label="Close"></button>
                                                        </div>
                                                        <!-- Form edit data disini -->
                                                        <div class="modal-body">
                                                            <div>
                                                                <div class="mb-3">
                                                                    <label for="namaMoU" class="form-label">Nama
                                                                        MoU</label>
                                                                    <input type="text" class="form-control" required
                                                                        id="namaMoU" placeholder="Masukkan Nama MoU" name="nama" value="{{ $data->nama }}">
                                                                </div>
                                                                <div class="mb-3">
                                                                    <div class="mb-3">
                                                                        <label for="jenisMoU" class="form-label">Status
                                                                            template</label>
                                                                        <select class="form-control select2"
                                                                            id="jenisMoU" name="is_active" required>
                                                                            <option>Pilih Status template</option>
                                                                            <option value="1" {{ $data->is_active ? 'selected' : '' }}>Aktif</option>
                                                                            <option value="0" {{ $data->is_active ? '' : 'selected' }}>Tidak Aktif</option>
                                                                        </select>
                                                                    </div>

                                                                    <div class="col-12">
                                                                        <div class="card">
                                                                            <div class="card-body">
                                                                                <h4 class="card-title">Unggah Template
                                                                                    MoU</h4>
                                                                                <div class="mb-5">
                                                                                    <div class="dropzone">
                                                                                        <div class="fallback">
                                                                                            <input name="file"
                                                                                                type="file"
                                                                                                >
                                                                                        </div>

                                                                                        <div class="dz-message needsclick">
                                                                                            <div class="mb-3">
                                                                                                <i
                                                                                                    class="mdi mdi-cloud-upload display-4 text-muted"></i>
                                                                                            </div>

                                                                                            <h4>Drop files here or click
                                                                                                to upload.</h4>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger waves-effect"
                                                                data-bs-dismiss="modal">Batal</button>
                                                            <button type="submit"
                                                                class="btn btn-primary waves-effect waves-light"><i
                                                                    class="fas fa-save"></i>
                                                                Simpan</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

<script>
    $(document).ready(function(){
    $("#datatable").DataTable({
        lengthChange: true
    });
});
</script>

@endpush
