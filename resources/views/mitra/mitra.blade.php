@extends('base')

@section('mitra')
<div class="col-md-8">
    <ol class="breadcrumb m-0">
        <li class="breadcrumb-item"><a href="#">SIMKERMA</a></li>
        <li class="breadcrumb-item"><a href="#">Mitra</a></li>
    </ol>
</div>
<div class="row align-items-center mb-0">
    <div class="col-md-12">
        <div class="float-end d-md-block">
            <div class="my-3 text-center">
                <button class="btn btn-primary" type="button" aria-expanded="false" onclick="window.location.href='/export-mitra'">
                    <i class="mdi mdi-file-export me-2"></i> Export
                </button>
            </div>
        </div>
    </div>
    <div class="row align-items-center mb-0">
        <div class="col-md-12">
            <div class="float-end d-md-block">
                <div class="my-3 text-center">
                    <a href="" class="btn btn-primary waves-effect waves-light"><i
                            class="mdi mdi-file-export me-1 "></i>
                        Export</a>
                    <button type="button" class="btn btn-primary waves-effect waves-light" data-bs-toggle="modal"
                        data-bs-target="#modelTambah">+ Tambahkan Mitra Baru</button>
                </div>

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="datatable" class="table table-bordered table-responsive mb-0">
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Nama Mitra</th>
                                <th>Jenis Mitra</th>
                                <th>Nama PJ</th>
                                <th>Alamat Email</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($mitra as $key => $data)
                            <tr>
                                <td>{{ $key + 1 }}</td>
                                <td>{{ $data->nama_mitra }}</td>
                                <td>{{ $data->jenis_mitra }}</td>
                                <td>{{ $data->penanggung_jawab }}</td>
                                <td>{{ $data->email }}</td>
                                <td>
                                    <button type="button" class="btn btn-warning waves-effect waves-light my-0" data-bs-toggle="modal" data-bs-target="#myModalEdit{{ $data->id }}"><i class="fa-solid fa-pencil"></i></button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    {{-- Modal Form Edit --}}
    @foreach($mitra as $data)
    <div id="myModalEdit{{ $data->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEdit{{ $data->id }}" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('mitra.update', $data->id) }}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="modal-header">
                        <h5 class="modal-title" id="myModalLabelEdit{{ $data->id }}">Edit Pengguna</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="col">
                            <div class="col-md-12 pb-3">
                                <label for="nama{{ $data->id }}" class="form-label">Nama Mitra</label>
                                <input type="text" class="form-control" id="nama{{ $data->id }}" name="nama_mitra" value="{{ $data->nama_mitra }}" autocomplete="off" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="jenis{{ $data->id }}" class="form-label">Jenis Mitra</label>
                                <input type="text" class="form-control" id="jenis{{ $data->id }}" name="jenis_mitra" value="{{ $data->jenis_mitra }}">
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="nama-pj{{ $data->id }}" class="form-label">Penanggung Jawab</label>
                                <input type="text" class="form-control" id="nama-pj{{ $data->id }}" name="penanggung_jawab" value="{{ $data->penanggung_jawab }}" autocomplete="off" required>
                            </div>
                            <div class="col-md-12 pb-3">
                                <label for="email{{ $data->id }}" class="form-label">Alamat Email</label>
                                <input type="email" class="form-control" id="email{{ $data->id }}" name="email2" value="{{ $data->email }}" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    @endforeach
</div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $("#datatable").DataTable({
                lengthChange: true
            });
        });
    </script>
@endpush