    {{-- Sidebar diberi logika untuk masing masing user --}}
    {{-- User superadmin : punya manajemen user --}}
    {{-- admin : tidak punya manajemen user --}}
    {{-- prodi : Input & View MOU --}}
    <div class="vertical-menu">

        <div data-simplebar class="h-100">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <!-- Left Menu Start -->
                <ul class="metismenu list-unstyled" id="side-menu">
                    <li class="menu-title">Main</li>

                    <li>
                        <a href="/admin-dashboard" class="waves-effect">
                            <i class="ti-home"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>

                    {{-- <li class="menu-title">Components</li> --}}

                    <li>
                        <a href="javascript: void(0);" class="has-arrow waves-effect">
                            <i class="ti-pie-chart"></i>
                            <span>Data Kerja Sama</span>
                        </a>
                        <ul class="sub-menu" aria-expanded="false">
                            <li><a href="/kerjasama">Semua Kerja Sama</a></li>
                            <li><a href="#">Kerja Sama Aktif</a></li>
                            <li><a href="#">Kerja Sama Berakhir</a></li>
                            <li><a href="#">Kerja Sama Akan Berakhir</a></li>
                        </ul>
                    </li>

                    <li>
                        <a href="/template" class="waves-effect">
                            <i class="ti-home"></i>
                            <span>Template MOU</span>
                        </a>
                    </li>

                    <li>
                        <a href="/mitra" class="waves-effect">
                            <i class="ti-pie-chart"></i>
                            <span>Mitra</span>
                        </a>
                    </li>

                    @auth
                        @if (Auth::user()->role === 'Super Admin')
                            <li>
                                <a href="/manajemen-pengguna" class="waves-effect">
                                    <i class="ti-view-grid"></i>
                                    <span>Pengguna</span>
                                </a>
                            </li>
                        @endif
                    @endauth

                    <li>
                        <a href="javascript: void(0);" class="has-arrow waves-effect">
                            <i class="ti-face-smile"></i>
                            <span>Referensi</span>
                        </a>
                        <ul class="sub-menu" aria-expanded="false">
                            <li><a href="/jenis-mitra">Jenis Mitra</a></li>
                            <li><a href="/status-mou">Status MOU</a></li>
                        </ul>
                    </li>

                    {{-- <li class="menu-title">Extras</li>

                        <li>
                            <a href="javascript: void(0);" class="has-arrow waves-effect">
                                <i class="ti-layout"></i>
                                <span>Layouts</span>
                            </a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li>
                                    <a href="javascript: void(0);" class="has-arrow">Vertical</a>
                                    <ul class="sub-menu" aria-expanded="true">
                                        <li><a href="layouts-light-sidebar.html">Light Sidebar</a></li>
                                        <li><a href="layouts-compact-sidebar.html">Compact Sidebar</a></li>
                                        <li><a href="layouts-icon-sidebar.html">Icon Sidebar</a></li>
                                        <li><a href="layouts-boxed.html">Boxed Layout</a></li>
                                        <li><a href="layouts-colored-sidebar.html">Colored Sidebar</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>


                        <li>
                            <a href="javascript: void(0);" class="has-arrow waves-effect">
                                <i class="ti-support"></i>
                                <span>  Extra Pages  </span>
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li><a href="pages-404.html">Error 404</a></li>
                                <li><a href="pages-500.html">Error 500</a></li>
                                <li><a href="pages-comingsoon.html">Coming Soon</a></li>
                                <li><a href="pages-faq.html">FAQs</a></li>
                            </ul>
                        </li>

                        <li>
                            <a href="javascript: void(0);" class="has-arrow waves-effect">
                                <i class="ti-more"></i>
                                <span>Multi Level</span>
                            </a>
                            <ul class="sub-menu" aria-expanded="true">
                                <li><a href="javascript: void(0);">Level 1.1</a></li>
                                <li><a href="javascript: void(0);" class="has-arrow">Level 1.2</a>
                                    <ul class="sub-menu" aria-expanded="true">
                                        <li><a href="javascript: void(0);">Level 2.1</a></li>
                                        <li><a href="javascript: void(0);">Level 2.2</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li> --}}

                </ul>
            </div>
            <!-- Sidebar -->
        </div>
    </div>
    <!-- Left Sidebar End -->
