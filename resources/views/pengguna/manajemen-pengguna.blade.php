@extends('base')

@section('admin')

<div class="row align-items-center mb-0">
    <div class="col-md-8">
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item"><a href="#">SIMKERMA</a></li>
            <li class="breadcrumb-item active" aria-current="page">Pengguna</li>
        </ol>
    </div>
</div>

<div class="tombol">
    <button class="btn btn-primary" type="button" aria-expanded="false" onclick="window.location.href='/export-users'">
        <i class="mdi mdi-file-export me-2"></i> Export
    </button>
    <button class="btn btn-primary mx-2" type="button" aria-expanded="false" data-bs-toggle="modal" data-bs-target="#myModalTambah">
        <i class="fa-solid fa-plus me-2"></i> Tambah Pengguna Baru
    </button>
</div>

<div class="row pt-3">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Pengguna</th>
                            <th>Program Studi</th>
                            <th>Alamat Email</th>
                            <th>Role</th>
                            <th>Status</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $key => $user)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $user->nama }}</td>
                            <td>{{ $user->prodi }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->role }}</td>
                            <td>
                                <div class="tmbl-tggl">
                                    <input type="checkbox" name="" id="toggle-status{{ $user->id }}" class="tggl-btn" data-user-id="{{ $user->id }}" {{ $user->is_active ? 'checked' : '' }}>
                                    <label for="toggle-status{{ $user->id }}" class="onbtn"><i class="fa-solid fa-check"></i></label>
                                    <label for="toggle-status{{ $user->id }}" class="offbtn"><i class="fa-solid fa-xmark"></i></label>
                                </div>
                            </td>
                            <td>
                                <button type="button" class="btn btn-warning waves-effect waves-light my-0" data-bs-toggle="modal" data-bs-target="#myModalEdit{{ $user->id }}"><i class="fa-solid fa-pencil"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div> <!-- end col -->
</div> <!-- end row -->

{{-- Modal Form Add --}}
<div id="myModalTambah" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelTambah" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('pengguna.manajemen-pengguna.store') }}" method="POST">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel2">Tambah Pengguna Baru</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="col">
                        <div class="col-md-12 pb-3">
                            <label for="nama-user" class="form-label">Nama Pengguna</label>
                            <input type="text" class="form-control" id="nama-user" name="nama" autocomplete="username" required>
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="prodi-user" class="form-label">Program Studi</label>
                            <input type="text" class="form-control" id="prodi-user" name="prodi">
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="email-user" class="form-label">Alamat Email</label>
                            <input type="email" class="form-control" id="email-user" name="email" autocomplete="username" required>
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="password-user" class="form-label">Password</label>
                            <input type="password" class="form-control" id="password-user" name="password" autocomplete="current-password" required>
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="role-user" class="form-label">Role</label>
                            <select class="form-select" aria-label="Default select example" id="role-user" name="role" required>
                                <option value="Admin">Admin</option>
                                <option value="Super Admin">Super Admin</option>
                                <option value="User">User</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Modal Form Edit --}}
@foreach($users as $user)
<div id="myModalEdit{{ $user->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabelEdit{{ $user->id }}" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="{{ route('pengguna.manajemen-pengguna.update', $user->id) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="modal-header">
                    <h5 class="modal-title" id="myModalLabel{{ $user->id }}">Edit Pengguna</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="col">
                        <div class="col-md-12 pb-3">
                            <label for="nama{{ $user->id }}" class="form-label">Nama Pengguna</label>
                            <input type="text" class="form-control" id="nama{{ $user->id }}" name="nama" value="{{ $user->nama }}" autocomplete="username" required>
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="prodi{{ $user->id }}" class="form-label">Program Studi</label>
                            <input type="text" class="form-control" id="prodi{{ $user->id }}" name="prodi" value="{{ $user->prodi }}">
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="email{{ $user->id }}" class="form-label">Alamat Email</label>
                            <input type="email" class="form-control" id="email{{ $user->id }}" name="email" value="{{ $user->email }}" autocomplete="username" required>
                        </div>
                        <div class="col-md-12 pb-3">
                            <label for="role{{ $user->id }}" class="form-label">Role</label>
                            <select class="form-select" aria-label="Default select example" id="role{{ $user->id }}" name="role" required>
                                <option value="Admin" {{ $user->role == 'Admin' ? 'selected' : '' }}>Admin</option>
                                <option value="Super Admin" {{ $user->role == 'Super Admin' ? 'selected' : '' }}>Super Admin</option>
                                <option value="User" {{ $user->role == 'User' ? 'selected' : '' }}>User</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary waves-effect" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary waves-effect waves-light">Submit</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endforeach

@endsection

@push('script')
<script>
    $(document).ready(function() {
        $(".tggl-btn").change(function(e) {
            e.preventDefault();
            var userId = $(this).data('user-id');
            var status = $(this).prop('checked') ? 1 : 0;
            Swal.fire({
                title: "Anda yakin?",
                text: "Aksi ini tidak dapat dibatalkan!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Ya, ubah!",
                cancelButtonText: "Batal"
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('update.status') }}",
                        data: {
                            _token: "{{ csrf_token() }}",
                            status: status,
                            userId: userId
                        },
                        success: function(response) {
                            Swal.fire({
                                title: "Sukses!",
                                text: "Status berhasil diubah.",
                                icon: "success"
                            }).then((result) => {
                                location.reload();
                            });
                        },
                        error: function(error) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Oops...',
                                text: 'Terjadi Kesalahan!',
                            });
                        }
                    });
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    Swal.fire(
                        'Batal',
                        'Tidak ada perubahan yang dilakukan.',
                        'info'
                    ).then((result) => {
                        location.reload();
                    });
                }
            });
        });
    });


$(document).ready(function(){
$("#datatable").DataTable({
    lengthChange: true
});
});

</script>
@endpush