<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SIMKERMA</title>
    <!-- Icon Title -->
    <link rel="shortcut icon" href="assets/images/simkerma-logo-small.png">
    <!-- Bootstrap Css -->
    <link href="assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <!-- CSS -->
    <link rel="stylesheet" href="assets/css/landing.css">
</head>
<body>
    <div class="container p-5">
        <h2 class="form-title text-center pb-5">Formulir Pengajuan Kerja Sama</h2>
        <form action="/send-datakerjasama" method="post" class="form"  enctype="multipart/form-data">
            @csrf
            <div class="row mb-4 justify-content-center">
                <label for="nama" class="col-sm-2 col-form-label">Nama Lengkap</label>
                <div class="col-sm-6">
                    <input class="form-control" name="penanggung_jawab" type="text" id="nama" placeholder="Masukkan nama lengkap">
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="email" class="col-sm-2 col-form-label">Alamat Email</label>
                <div class="col-sm-6">
                    <input class="form-control" name="email" type="email" id="email" placeholder="Masukkan alamat email">
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="instansi" class="col-sm-2 col-form-label">Nama Instansi</label>
                <div class="col-sm-6">
                    <input class="form-control" name="nama_mitra" type="text" id="instansi" placeholder="Masukkan nama instansi">
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="kegiatan" class="col-sm-2 col-form-label">Nama Kegiatan</label>
                <div class="col-sm-6">
                    <input class="form-control" name="deskripsi" type="text" id="kegiatan" placeholder="Masukkan nama kegiatan">
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="nomor_surat_mitra" class="col-sm-2 col-form-label">No. Surat Mitra</label>
                <div class="col-sm-6">
                    <input class="form-control" name="no_surat_mitra" type="text" id="nomor_surat_mitra" placeholder="Masukkan nomor surat mitra ">
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="tgl-mulai" class="col-sm-2 col-form-label">Tanggal Mulai</label>
                <div class="col-sm-6">
                    <input class="form-control" name="tanggal_mou" type="date" id="tgl-mulai" placeholder="Pilih tanggal mulai">
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="tgl-akhir" class="col-sm-2 col-form-label">Tanggal Berakhir</label>
                <div class="col-sm-6">
                    <input class="form-control" name="tanggal_akhir" type="date" id="tgl-akhir" placeholder="Pilih tanggal berakhir">
                </div>
            </div>
            <div class="row mb-4 justify-content-center">
                <label for="doc-mou" class="col-sm-2 col-form-label">Dokumen MoU</label>
                <div class="col-sm-6">
                    <div class="file-upload-wrapper" data-text="Pilih dokumen MoU">
                        <input name="file" type="file" class="file-upload-field" value="">
                    </div>
                </div>
            </div>
            <div class="button-group d-flex justify-content-end gap-2 pt-5">
                <button type="button" class="btn btn-batal fw-bold">Batal</button>
                <button type="submit"  class="btn btn-kirim fw-bold">Kirim</button>
            </div>
        </form>

    </div>

        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <script>
        $("form").on("change", ".file-upload-field", function () {
        $(this)
            .parent(".file-upload-wrapper")
            .attr(
            "data-text",
            $(this)
                .val()
                .replace(/.*(\/|\\)/, "")
            );
        });

    </script>
    @if(session('success'))
        <script>
            alert("Data berhasil diajukan")
        </script>
    @endif
    @if(session('error'))
        <script>
            alert("{{ session('error') }}")
        </script>
    @endif
</body>
</html>