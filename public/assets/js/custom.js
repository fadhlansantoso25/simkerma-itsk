// Morris Chart
const aset = [
    { year: 2015, instansi: 100, organisasi: 90 },
    { year: 2020, instansi: 50, organisasi: 40 },
    { year: 2025, instansi: 20, organisasi: 50 },
    { year: 2030, instansi: 10, organisasi: 20 },
    { year: 2035, instansi: 40, organisasi: 60 },
    { year: 2040, instansi: 60, organisasi: 30 }
  ];
  
  Morris.Bar({
    element: 'chart',
    data: aset,
    xkey: 'year',
    ykeys: ['instansi', 'organisasi'],
    labels: ['Instansi', 'Organisasi'],
    hideHover: 'auto',
    barColors: ['#3C4CCF', '#02A499'],
    stacked: false
  });
  
