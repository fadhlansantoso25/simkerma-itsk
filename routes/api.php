<?php

use App\Http\Controllers\KerjaSamaController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/Data-Kerjasama',[KerjaSamaController::class,'getDataAktif']);
Route::get('/Data-Akan-Berakhir',[KerjaSamaController::class, 'getDataAkanBerakhir']);
Route::put('/Edit-DataKerjasama/{id}',[KerjaSamaController::class, 'update']);
