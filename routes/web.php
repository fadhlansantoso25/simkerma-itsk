<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\KerjaSamaController;
use App\Http\Controllers\LandingController;
use App\Http\Controllers\SuperAdminController;
use App\Http\Controllers\ViewController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MitraController;
use App\Http\Controllers\TemplateController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('landingpage.index');
});
Route::get('/form', [LandingController::class, 'showForm']);

// Route untuk guest
Route::middleware('IsLogin')->group(function () {
    Route::get('/login', [LoginController::class, 'create']);
    Route::post('/session/login', [LoginController::class, 'login']);

    Route::get('lupa-password', [PasswordResetLinkController::class, 'create'])->name('lupa-password');
    Route::post('lupa-password', [PasswordResetLinkController::class, 'store']);

    Route::get('reset-password/{token}', [NewPasswordController::class, 'create'])->name('password.reset');
    Route::post('reset-password', [NewPasswordController::class, 'store'])->name('password.store');
});

Route::post('/add-datakerjasama', [KerjaSamaController::class, 'storeAdmin']);
Route::post('/send-datakerjasama', [KerjaSamaController::class, 'store']);

// Route untuk manajemen pengguna khusus superadmin
Route::middleware('CheckSuperAdmin')->group(function () {
    Route::get('/superadmin-dashboard', [SuperAdminController::class, 'showSuperAdmin'])->name('admin.dashboard');
    Route::get('/manajemen-pengguna', [UserController::class, 'index'])->name('pengguna.manajemen-pengguna');
    Route::get('/manajemen-pengguna/tambah', [UserController::class, 'create'])->name('pengguna.manajemen-pengguna.tambah');
    Route::post('/manajemen-pengguna', [UserController::class, 'store'])->name('pengguna.manajemen-pengguna.store');
    Route::get('/manajemen-pengguna/{user}/edit', [UserController::class, 'edit'])->name('pengguna.manajemen-pengguna.edit');
    Route::put('/manajemen-pengguna/{user}', [UserController::class, 'update'])->name('pengguna.manajemen-pengguna.update');
    Route::get('/manajemen-pengguna/{user}', [UserController::class, 'show'])->name('pengguna.manajemen-pengguna.show');
    Route::post('/update-status', [UserController::class, 'updateStatus'])->name('update.status');
    Route::get('/export-users', [UserController::class, 'exportUsers']);
    Route::delete('/kerjsama/${id}',[KerjaSamaController::class, "delete"])->name('data.destroy');
});

//Route untuk admin
Route::middleware('CheckAdmin')->group(function () {
    Route::get('/admin-dashboard', [AdminController::class, 'showAdmin'])->name('CheckAdmin');
});

Route::get('/hello', [ViewController::class, 'showHello']);

// Route untuk data kerjasama
Route::get('/kerjasama', [AdminController::class, 'showKerjasama']);

// Route untuk jenis mitra direferensi
Route::get('/jenis-mitra', function () {
    return view('referensi.admin-jenis-mitra');
});

// Route untuk status mitra direferensi
Route::get('/status-mou', function () {
    return view('referensi.admin-status-mou');
});

// Route untuk mitra
Route::get('/mitra', [MitraController::class, 'index'])->name('mitra.index');
Route::get('/mitra/{mitra}', [MitraController::class, 'show'])->name('mitra.show');
Route::get('/mitra/{mitra}/edit', [MitraController::class, 'edit'])->name('mitra.edit');
Route::put('/mitra/{mitra}', [MitraController::class, 'update'])->name('mitra.update');
Route::get('/export-mitra', [MitraController::class, 'exportMitra'])->name('mitra.export');


Route::apiResource('template', TemplateController::class)->except(['show', 'destroy']);
Route::post('template/toggle-status/{template}', [TemplateController::class, 'toggleStatus'])->name('template.toggle-status');

Route::get('/logout', [LoginController::class, 'logout']);
