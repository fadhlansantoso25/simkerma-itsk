<?php

namespace App\Http\Controllers;

use App\Models\DataKerjasama;
use Carbon\Carbon;
use Illuminate\Auth\Events\Validated;
use Illuminate\Contracts\Support\ValidatedData;
use Illuminate\Http\Request;

class KerjaSamaController extends Controller
{
    public function store(Request $request){
        try{
            $validatedData = $request->validate([
                'penanggung_jawab' => 'required',
                'email' => 'required|email',
                'nama_mitra' => 'required',
                'deskripsi' =>  'required',
                'tanggal_mou' => 'required|date',
                'tanggal_akhir' => 'required|date',
                'file' => 'required|file',
                'no_surat_mitra' => 'required'
            ]);
            
            $file =  $request->file('file');
            $filename = $file->getClientOriginalName();
            $fileExt = $file->getClientMimeType();
            if($fileExt != 'application/pdf'){
                throw new \Exception('Format Dokumen Tidak Sesuai', 400);
            }
            $file->move(public_path('Data-Kerjasama'), $filename);
            
            $validatedData['file']= '/Data-Kerjasama/' . $filename;
            $validatedData['jenis_mitra']= '';
            $validatedData['no_surat_instansi']= '';
            // $validatedData['no_surat_mitra']= '';
            $validatedData['jenis_dokumen']= '';
            $validatedData['status']= 'Pengajuan';
            
            DataKerjasama::create($validatedData);
            
            return  redirect('/form')->with('success', 'Data berhasil disimpan');
        }
        catch(\Illuminate\Validation\ValidationException $e) {
            return redirect('/form')->withErrors($e->validator->errors())->withInput();
        }
        catch(\Exception $e){
            return redirect('/form')->with('error',$e->getMessage())->withInput();
        }
    }

    public function storeAdmin(Request $request){
        try{
            $validatedData = $request->validate([
                'penanggung_jawab' => 'required',
                'email' => 'required|email',
                'nama_mitra' => 'required',
                'deskripsi' =>  'required',
                'tanggal_mou' => 'required|date',
                'tanggal_akhir' => 'required|date',
                'file' => 'required|file',
                'no_surat_mitra' => 'required'
            ]);
            
            // dd($request->all());
            $file =  $request->file('file');
            $filename = $file->getClientOriginalName();
            $fileExt = $file->getClientMimeType();
            if($fileExt != 'application/pdf'){
                throw new \Exception('Format Dokumen Tidak Sesuai', 400);
            }
            $file->move(public_path('Data-Kerjasama'), $filename);
            
            $validatedData['file']= '/Data-Kerjasama/' . $filename;
            $validatedData['jenis_mitra']= '';
            $validatedData['no_surat_instansi']= '';
            // $validatedData['no_surat_mitra']= '';
            $validatedData['jenis_dokumen']= '';
            $validatedData['status']= 'Pengajuan';
            
            DataKerjasama::create($validatedData);
            
            return  redirect('/kerjasama')->with('success', 'Data berhasil disimpan');
        }
        catch(\Illuminate\Validation\ValidationException $e) {
            return redirect('/kerjasama')->withErrors($e->validator->errors())->withInput();
        }
        catch(\Exception $e){
            return redirect('/kerjasama')->with('error',$e->getMessage())->withInput();
        }
    }
    public function getDataAktif(){
        $DataKerjaSama = DataKerjasama::all();

        return response()->json($DataKerjaSama);
    }
    public function getDataAkanBerakhir(){
        $DataKerjaSama = DataKerjasama::all();

        $DataAkanBerakhir = $DataKerjaSama->filter(function($item){
            $tanggalAkhir = Carbon::parse($item->tanggal_akhir);

            $selisih = $tanggalAkhir->diffInDays(now());

            return $selisih <= 7;
        });

        return response()->json($DataAkanBerakhir);
    }

    //not finished
    public function update(Request $request,$id){

        $validatedData = $request->validate([
            'penanggung_jawab' => 'required',
            'email' => 'required|email|',
            'nama_mitra' => 'required',
            'deskripsi' =>  'required',
            'tanggal_mou' => 'required|date',
            'tanggal_akhir' => 'required|date',
            // 'file' => 'required|file',
            'jenis_mitra' => 'required',
            'no_surat_instansi' => 'required',
            'no_surat_mitra' => 'required',
            'jenis_dokumen' => 'required',
            'status' => 'required'
        ]);
        
        $find = DataKerjasama::findOrFail($id);
        
        $find->update($validatedData);
        
        return response()->json(['message' => 'Data berhasil diperbarui']);

    }
    public function delete($id){
        $find = DataKerjasama::findOrFail($id);
        if($find){
            // dd(response()->json($find));
            $find->delete();
            return redirect('/kerjasama')->with('success','Data Berhasil Dihapus');
        }
        return redirect('/')->with('error','error data tidak ditemukan');

    }
}
