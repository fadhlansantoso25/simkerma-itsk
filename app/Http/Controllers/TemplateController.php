<?php

namespace App\Http\Controllers;

use App\Http\Requests\TemplateRequest;
use App\Models\Template;
use Illuminate\Http\Request;

class TemplateController extends Controller
{
    public function index(Request $request)
    {
        $template = Template::get();
        return view('template.template', compact('template'));
    }

    public function store(TemplateRequest $request)
    {
        $file = $request->file('file');

        $fileName = $file->hashName();
        $file->move(public_path('Data-Template'), $fileName);

        $request->user()->template()->create([
            ...$request->validated(),
            'file' => 'Data-Template/' . $fileName
        ]);

        return redirect(route('template.index'))->with('success', 'Data Template Berhasil di tambahkan');
    }

    public function update(TemplateRequest $request, Template $template)
    {
        $fileName = $template->file;
        if($request->hasFile('file')){
            unlink(public_path($fileName));

            $file = $request->file('file');
            $fileName = $file->hashName();
            $file->move(public_path('Data-Template'), $fileName);

            $fileName = 'Data-Template/' . $fileName;
        }
        $template->update([
            ...$request->validated(),
            'file' => $fileName
        ]);

        return redirect(route('template.index'))->with('success', 'Data Template Berhasil di edit');
    }

    public function toggleStatus(Template $template)
    {
        $template->is_active = !$template->is_active;
        $template->save();

        return response()->json(['success' => true]);
    }
}
