<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('pengguna.manajemen-pengguna', compact('users'));
    }

    public function exportUsers()
    {

        $users = User::all();

        (new FastExcel($users))->export('data-users.xlsx', function ($user) {
            return [
                'ID' => $user->id,
                'Nama' => $user->nama,
                'Email' => $user->email,
                'Prodi' => $user->prodi,
                'Role' => $user->role,
            ];
        });

        return response()->download('data-users.xlsx')->deleteFileAfterSend();
    }
    
    public function show(User $user)
    {
        return view('pengguna.manajemen-pengguna-show', compact('user'));
    }

    public function create()
    {
        return view('pengguna.manajemen-pengguna-create');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'prodi' => 'nullable|max:255',
            'role' => 'required|max:255',
        ]);

        $validatedData['is_active'] = true;
        $validatedData['password'] = bcrypt($request->password);

        User::create($validatedData);

        return redirect('/manajemen-pengguna')->with('success', 'Pengguna berhasil ditambah!');
    }

    public function edit(User $user)
    {
        return view('pengguna.manajemen-pengguna-edit', compact('user'));
    }

    public function update(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'nama' => 'required|max:255',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'prodi' => 'nullable|max:255',
            'role' => 'required|max:255',
        ]);

        $user->update($validatedData);

        return redirect('/manajemen-pengguna');
    }

    public function updateStatus(Request $request)
    {
        $userId = $request->input('userId');

        $user = User::findOrFail($userId);
        $user->is_active = !$user->is_active;
        $user->save();

        return response()->json(['success' => true]);
    }
}
