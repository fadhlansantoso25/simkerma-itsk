<?php

namespace App\Http\Controllers;

use App\Models\DataKerjasama;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function showAdmin(){
        return view ('dashboard/admin-dashboard');
    }

    public function showKerjasama(){
        $dataKerjasama = DataKerjasama::get();
        return view ('data-kerja-sama/admin-data-kerjasama', compact('dataKerjasama'));
    }
}
