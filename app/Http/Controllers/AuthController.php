<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function showLogin(){
        return view('auth/login');
    }
    public function showEmail() {
        return view('auth/password/email');
    }
    public function showReset() {
        return view('auth/password/reset');
    }
}
