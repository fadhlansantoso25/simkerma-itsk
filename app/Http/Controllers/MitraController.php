<?php

namespace App\Http\Controllers;

use App\Models\DataKerjasama;
use Illuminate\Http\Request;
use Rap2hpoutre\FastExcel\FastExcel;

class MitraController extends Controller
{
    public function index()
    {
        $mitra = DataKerjasama::select('id', 'nama_mitra', 'jenis_mitra', 'penanggung_jawab', 'email')->get();
        return view('mitra.mitra', compact('mitra'));
    }

    public function exportMitra()
    {
        $mitra = DataKerjasama::select('id', 'nama_mitra', 'jenis_mitra', 'penanggung_jawab', 'email')->get();

        (new FastExcel($mitra))->export('data-mitra.xlsx', function ($mitra) {
            return [
                'ID' => $mitra->id,
                'Nama Mitra' => $mitra->nama_mitra,
                'Jenis Mitra' => $mitra->jenis_mitra,
                'Nama PJ' => $mitra->penanggung_jawab,
                'Email' => $mitra->email,
            ];
        });

        return response()->download('data-mitra.xlsx')->deleteFileAfterSend();
    }

    public function show(DataKerjasama $mitra)
    {
        return view('mitra.mitra-show', compact('mitra'));
    }

    public function edit(DataKerjasama $mitra)
    {
        return view('mitra.mitra-edit', compact('mitra'));
    }

    public function update(Request $request, DataKerjasama $mitra)
    {
        $validatedData = $request->validate([
            'nama_mitra' => 'required|max:255',
            'jenis_mitra' => 'required|max:255',
            'penanggung_jawab' => 'required|max:255',
            'email' => 'required|email|unique:data_kerjasama,email,' . $mitra->id,
        ]);

        $mitra->update($validatedData);

        return redirect('/mitra');
    }
}
